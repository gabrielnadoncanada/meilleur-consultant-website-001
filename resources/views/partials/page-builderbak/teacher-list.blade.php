
@php
  $args = array('numberposts' => -1, 'post_type' => 'teacher');
  $teachers = get_posts( $args );
@endphp

<div class="container-fluid container">
  @if($block['title'])
    <div class="row">
      <div class="col-sm-12">
        <h2>{{$block['title']}}</h2>
      </div>
    </div>
  @endif
  <div class="card-group">
    @foreach($teachers as $teacher)
      @php
        $id = $teacher->ID
      @endphp
      <div class="card col-sm-12 col-md-6">
        <img
          src="{{get_field('image', $id) ?  get_field('image', $id) : 'https://via.placeholder.com/350x350'}}"
          alt="" class="card-img-top"
        >
        <div class="card-body">
          <h5 class="card-title">{{$teacher->post_title}}</h5>
          <div class="accordion" id="accordion_teacher_{{$id}}">
            @php
              $fields = array('certifications','formations', 'experiences', 'technologies', 'services');
              $fields_array = array();
              foreach($fields as $field) {
                array_push($fields_array, get_field($field, $id));
              }
            @endphp
            @foreach($fields_array as $key => $field_array)
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button
                      class="btn btn-link btn-block text-left"
                      type="button"
                      data-toggle="collapse"
                      data-target="#collapse_teacher_{{$id}}_{{$key}}"
                      aria-expanded="false"
                      aria-controls="collapse_teacher_{{$id}}_{{$key}}">
                      {{ $fields[$key] }}
                    </button>
                  </h2>
                </div>

                <div id="collapse_teacher_{{$id}}_{{$key}}" class="collapse" aria-labelledby="headingOne"
                     data-parent="#accordion_teacher_{{$id}}">
                  <div class="card-body">
                    <ul>
                      @foreach($field_array as $field)
                        @if(!empty($field['value']))
                          <li>
                            {{ __($field['value'], 'teacher') }}
                          </li>
                        @endif
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            @endforeach


          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>

