
<div class="container-fluid container">
  <div class="row">
    <div class="col-sm-12 col-md-8 mx-auto text-center">
      <div class="mb-4">
        @if($block['title'])
          <h2 class="h4">{{ $block['title']  }}</h2>
        @endif

        @if($block['subtitle'])
          <h3 class="h5">{{ $block['subtitle'] }}</h3>
        @endif
      </div>
      <div class="mb-3">
        @if($block['content'])
          <p class="h6">{{ $block['content'] }}</p>
        @endif
      </div>
      <div>
        @if($block['button']['url'] && $block['button']['label'])
          <a class="btn " href="{{$block['button']['url']}}">{{$block['button']['label']}}</a>
        @endif
      </div>
    </div>
  </div>
</div>


