
@php
  $args = array('numberposts' => -1, 'post_type' => 'partner');
  $partners = get_posts( $args );
@endphp

<div class="container-fluid container">
  @if($block['title'])
    <div class="row">
      <div class="col-sm-12">
        <h2>{{$block['title']}}</h2>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-sm-12">
      <div class="slick">
        @foreach($partners as $partner)
          @php
            $id = $partner->ID;
          @endphp

          <div>
            <img src="{{ get_field('image', $id) ?  get_field('image', $id) : 'https://via.placeholder.com/100x100'}}" alt=""
                 class="img-fluid">
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>

