

<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-lg-10 mx-auto">
        <div class="row {{ $block['img_right'] ? 'flex-row-reverse' : '' }}">
          <div class="col-sm-12 pr-md-5 col-md-5 col-lg-4 mb-5 text-center">
            <img src="{{$block['image'] ?  $block['image'] : 'https://via.placeholder.com/300x300'}}" alt=""
                 class="img-fluid">
          </div>
          <div class="col-sm-12 col-md-7 col-lg-8 mt-xl-3 text-center text-md-left">
            @if($block['title'])
              <h3>{{ $block['title']  }}</h3>
            @endif

            @if($block['subtitle'])
              <h4>{{ $block['subtitle'] }}</h4>
            @endif

            @if($block['content'])
              <p class="mb-5">{{ $block['content'] }}</p>
            @endif

            @if($block['button']['url'] && $block['button']['label'])
              <a class="btn float-md-right" href="{{$block['button']['url']}}">{{$block['button']['label']}}</a>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
