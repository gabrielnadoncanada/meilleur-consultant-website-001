@if($block['title'])
  <h3>{{ $block['title']  }}</h3>
@endif

@if($block['subtitle'])
  <h4>{{ $block['subtitle'] }}</h4>
@endif

@if($block['content'])
  <p>{{ $block['content'] }}</p>
@endif

@if($block['button']['url'] && $block['button']['label'])
  <a class="btn btn-primary" href="{{$block['button']['url']}}">{{$block['button']['label']}}</a>
@endif
