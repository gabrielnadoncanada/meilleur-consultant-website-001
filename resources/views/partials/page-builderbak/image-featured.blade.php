
<div class="row {{ $block['img_right'] ? 'flex-row-reverse' : '' }} mx-0">
  <div class="image col-sm-12 col-md-6 px-0">
    <img src="{{$block['image'] ?  $block['image'] : 'https://via.placeholder.com/960x590'}}" alt="" class="img-fluid">
  </div>
  <div class="content col-sm-12 col-md-6">
    <div class="row">
      <div class="col-12 col-lg-10">
        <div class="mb-4">
          @if($block['title'])
            <h2 class="h4">{{ $block['title']  }}</h2>
          @endif

          @if($block['subtitle'])
            <h3 class="h5">{{ $block['subtitle'] }}</h3>
          @endif
        </div>
        <div>
          @if($block['content'])
            <p>{{ $block['content'] }}</p>
          @endif
        </div>
        <div>
          @if($block['button']['url'] && $block['button']['label'])
            <a class="btn" href="{{$block['button']['url']}}">{{$block['button']['label']}}</a>
          @endif
        </div>



      </div>
    </div>


  </div>
</div>
