
<div class="container-fluid px-0">
  <div class="slick">
    @foreach($block['slides'] as $block)
      <div class="slide">

        <img src="{{$block['image'] ?  $block['image'] : 'https://via.placeholder.com/1920x870'}}" alt="" class="img-fluid">

        <div class="slide-content">
          <div class="row">
            <div class="col-sm-12 col-md-10 text-left">
              @if($block['title'])
                <h2 class="h1">{{ $block['title']  }}</h2>
              @endif

              @if($block['subtitle'])
                <h4>{{ $block['subtitle'] }}</h4>
              @endif

              @if($block['content'])
                <p>{{ $block['content'] }}</p>
              @endif

              @if($block['button']['url'] && $block['button']['label'])
                <a class="btn" href="{{$block['button']['url']}}">{{$block['button']['label']}}</a>
              @endif
            </div>

          </div>

        </div>
      </div>
    @endforeach
  </div>

</div>
