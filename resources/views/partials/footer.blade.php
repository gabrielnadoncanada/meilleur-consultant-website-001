<section id="contact-form" class="hero bg-purple" style="">
  <div class="container-fluid container">
    <div class="row reveal">
      <div class="col-sm-12 text-center">
        <img class="form-logo" src="{{\App\asset_path('images/icon-logo.svg')}}" alt="icon logo">
        <h2>{{ __('Contactez Meilleur Consultant', 'base') }}
        </h2>
        {!! do_shortcode('[contact-form-7 id="5" title="Contact"]') !!}
      </div>
    </div>
  </div>
</section>


<footer class="content-info">
  <div class="container-fluid">
    <div class="row">
      @if (get_field('footer_logo', 'option'))
        <div class="col-sm-12 col-md-4 mb-4 ">
          <a class="brand" href="{{ home_url('/') }}">
            <img class="img-fluid" src="{{ get_field('footer_logo', 'option')['url'] }}" alt="logo">
          </a>
        </div>
      @endif

      <div class="col-sm-12 offset-md-1 col-md-7">
        @if (has_nav_menu('footer_navigation'))
          {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </div>
    </div>

    <div class="row copyright">
      <div class="col-sm-12">
        @if (get_field('copyright', 'option'))
          {!! get_field('copyright', 'option') !!}
        @endif
      </div>
    </div>
  </div>
</footer>










