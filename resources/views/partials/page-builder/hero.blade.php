<div class="container-fluid ">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 reveal">
        @if($block['title'])
          @php
            $element = '<';
            $element .= $block['title_tag'];
            $element .= $block['title_tag_class'] ? ' class="'.$block['title_tag_class'].'"' : '';
            $element .= '>';
            $element .=  $block['title'];
            $element .= '</'.$block['title_tag'].'>';

            echo $element;
          @endphp
        @endif
        @if($block['subtitle'])
          @php
            $element = '<';
            $element .= $block['subtitle_tag'];
            $element .= $block['subtitle_tag_class'] ? ' class="'.$block['subtitle_tag_class'].'"' : '';
            $element .= '>';
            $element .=  $block['subtitle'];
            $element .= '</'.$block['subtitle_tag'].'>';

            echo $element;
          @endphp
        @endif
        @if($block['content'])
          {!! $block['content'] !!}
        @endif
        @if($block['url'] && $block['label'])
          <a class="btn btn-primary" href="{{$block['url']}}">{!! $block['label']!!}</a>
        @endif
      </div>
    </div>
  </div>

</div>


