<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 reveal">
        <div class="row {{ $block['img_right'] ? 'flex-row-reverse' : '' }}">
          <div class="col-sm-12 col-md-5">
            <img src="{{$block['image'] ?  $block['image'] : 'https://via.placeholder.com/300x300'}}" alt=""
                 class="img-fluid">
          </div>
          <div class="col-sm-12 col-md-7 ">

            @if($block['title'])
              @php
                echo '<'.$block['title_tag'].' class="'.$block['title_tag_class'].'">'.$block['title'].'</'.$block['title_tag'].'>';
              @endphp
            @endif
            @if($block['subtitle'])
              @php
                echo '<'.$block['subtitle_tag'].' class="'.$block['subtitle_tag_class'].'">'.$block['subtitle'].'</'.$block['subtitle_tag'].'>';
              @endphp
            @endif


            @if($block['content'])
              <p>{!! $block['content'] !!}</p>
            @endif


            @if($block['url'] && $block['label'])
              <a class="btn btn-primary" href="{{$block['url']}}">{!!$block['label']!!}</a>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
