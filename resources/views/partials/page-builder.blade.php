@if($page_builder)
  @foreach($page_builder as $key => $block)
    @if(!$block['hidden'])
      <section
        id="{{ strtolower($block['acf_fc_layout']) . '_' . $key }}"
        class="{{$block['acf_fc_layout']}} {{ $block['background-color'] ? 'bg-' . $block['background-color'] : '' }}"
        style="{{ $block['background-image'] ? 'background-image: url("'. $block['background-image'] .'");' : '' }}"
      >
        @include('partials.page-builder.' . str_replace('_', '-', $block['acf_fc_layout']))
      </section>
    @endif
  @endforeach
@endif
