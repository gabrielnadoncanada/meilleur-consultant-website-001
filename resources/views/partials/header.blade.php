<header class="banner">
  <div class="primary-nav">
    <div class="container-fluid px-0">

      <div class="row ">
        <div class="col d-flex align-items-center">
          <div class="ml-4">
            @if (get_field('logo', 'option'))
              <a class="brand" href="{{ home_url('/') }}">
                <img class="img-fluid" src="{{ get_field('logo', 'option')['url'] }}" alt="logo">
              </a>
            @else
              <a class="brand" href="{{ home_url('/') }}"> {{ get_bloginfo('name', 'display') }}</a>
            @endif
          </div>
        </div>
        <div class="col col-md-9 d-flex justify-content-end align-items-center">
          <nav class="nav-primary navbar navbar-expand-lg">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              @if (get_field('footer_logo', 'option'))

                <div class="ml-4 d-lg-none">
                  <a class="brand" href="{{ home_url('/') }}">
                    <img class="img-fluid" src="{{ get_field('footer_logo', 'option')['url'] }}" alt="logo">
                  </a>
                </div>


              @endif
              @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
              @endif
            </div>
          </nav>
        </div>

      </div>
    </div>
  </div>

  <div class="secondary-nav bg-secondary">
    <div class="container-fluid ">
      <div class="row justify-content-lg-end ml-1 ml-lg-0 align-items-center">
        @if (get_field('mail', 'option'))
          <a target="_blank" href="mailto:{{ get_field('mail', 'option') }}">
            <img src="{{ \App\asset_path('images/mail-icon.svg') }}" alt="mail-icon">
            {{ get_field('mail', 'option') }}
          </a>
        @endif
        @if (get_field('phone', 'option'))
          <a target="_blank" href="tel:{{ get_field('phone', 'option') }}">
            <img src="{{ \App\asset_path('images/phone-icon.svg') }}" alt="phone-icon">
            {{ get_field('phone', 'option') }}
          </a>
        @endif

      </div>
    </div>

  </div>
</header>
