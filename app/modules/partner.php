<?php

namespace App;

class partner
{

    public function __construct()
    {
        add_action("init", [$this, "init"]);
    }

    public function init()
    {

        $labels = array(
            'name' => __('Partenaires', 'partner'),
            'singular_name' => __('Partenaire', 'partner'),
            'menu_name' => __('Partenaires', 'partner'),
            'all_items' => __('Tout les Partenaires', 'partner'),
            'view_item' => __('Voir tout les Partenaires', 'partner'),
            'add_new_item' => __('Ajouter un nouveau Partenaire', 'partner'),
            'add_new' => __('Ajouter', 'partner'),
            'edit_item' => __('Éditer', 'partner'),
            'update_item' => __('Modifier', 'partner'),
            'search_items' => __('Rechercher', 'partner'),
            'not_found' => __('Aucun résultat', 'partner'),
            'not_found_in_trash' => __('Introuvable dans la poubelle', 'partner'),
        );

        $args = array(
            'label' => __('Partenaire', 'partner'),
            'description' => __('Partenaires', 'partner'),
            'labels' => $labels,
            'rewrite' => ['slug' => __('partenaires', 'partner')],
            'supports' => array('title'),
            'show_in_rest' => false,
            'hierarchical' => false,
            'public' => true,
            'has_archive' => true,
        );
        register_post_type('partner', $args);
    }


}
new partner();
