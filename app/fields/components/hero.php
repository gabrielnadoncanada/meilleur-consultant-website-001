<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('hero');

$component
    ->addTab('title')
    ->addTextarea('title')
    ->setConfig('new_lines', 'br')
    ->addSelect('title_tag', [
        'label' => 'Select tag',
        'instructions' => '',
        'choices' => ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        'default_value' => ['h1'],
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
        'wrapper' => $config->wrapper
    ])
    ->addText('title_tag_class', ['wrapper' => $config->wrapper])
    ->addTab('subtitle')
    ->addTextarea('subtitle')
    ->setConfig('new_lines', 'br')
    ->addSelect('subtitle_tag', [
        'label' => 'Select tag',
        'instructions' => '',
        'choices' => ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
        'default_value' => ['h2'],
        'allow_null' => 0,
        'multiple' => 0,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
        'wrapper' => $config->wrapper
    ])
    ->addText('subtitle_tag_class', ['wrapper' => $config->wrapper])

    ->addTab('content')
    ->addWysiwyg('content')
    ->setConfig('new_lines', 'br')
    ->addTab('button')
    ->addText('label', ['wrapper' => $config->wrapper])
    ->addUrl('url', ['label' => 'URL', 'wrapper' => $config->wrapper])
    ->addTab('background')
    ->addSelect('background-color', [
        'label' => 'Select theme',
        'instructions' => '',
        'choices' => ['primary', 'secondary', 'purple', 'gray'],
        'default_value' => [],
        'allow_null' => 1,
        'multiple' => 0,
        'ui' => 0,
        'ajax' => 0,
        'return_format' => 'value',
    ])
    ->addImage('background-image', ['return_format' => 'url', 'wrapper' => $config->wrapper])
    ->addTab('general')
    ->addTrueFalse('hidden');


return $component;
