<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$partner = new FieldsBuilder('partner');

$partner
    ->setLocation('post_type', '==', 'partner');

$partner
    ->addImage('image', ['return_format' => 'url'])
    ->addUrl('url', ['label' => 'URL']);

return $partner;
