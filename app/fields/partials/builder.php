<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$builder = new FieldsBuilder('builder');


$builder
    ->addTab('builder')
    ->addFlexibleContent('page_builder', ['button_label' => 'Add Component'])
    ->addLayout(get_field_partial('components.hero'))
    ->addLayout(get_field_partial('components.text-image-featured'));

return $builder;
