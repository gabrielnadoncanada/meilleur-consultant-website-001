<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('text_image_featured');

$component
    ->addTrueFalse('hidden')
    ->addTab('image')
    ->addImage('image', ['return_format' => 'url','wrapper' => $config->wrapper])
    ->addTrueFalse('img_right', [
        'label' => 'Image à droite',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => $config->wrapper,
        'message' => '',
        'default_value' => 0,
        'ui' => 0,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addTab('content')
    ->addText('title')
    ->addText('subtitle')
    ->addTextarea('content')
    ->addTab('button')
    ->addGroup('button')
        ->addText('label', ['wrapper' => $config->wrapper])
        ->addUrl('url', ['label' => 'URL', 'wrapper' => $config->wrapper])
    ->endGroup();


return $component;
