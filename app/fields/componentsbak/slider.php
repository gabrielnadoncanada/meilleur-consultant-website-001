<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('Slider');

$component
    ->addTrueFalse('hidden')
    ->addRepeater('slides', ['min' => 1, 'layout' => 'row'])
        ->addImage('image', ['return_format' => 'url'])
        ->addText('title')
        ->addText('subtitle')
        ->addTextarea('content')
        ->addGroup('button')
            ->addText('label', ['wrapper' => $config->wrapper])
            ->addUrl('url', ['label' => 'URL', 'wrapper' => $config->wrapper])
        ->endGroup()
    ->endRepeater();


return $component;
