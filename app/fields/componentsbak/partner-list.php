<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('partner_list');

$component
    ->addTrueFalse('hidden')
    ->addText('title');


return $component;
