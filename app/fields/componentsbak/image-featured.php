<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('image_featured');

$component
    ->addTrueFalse('hidden')
    ->addImage('image', ['return_format' => 'url'])
    ->addTrueFalse('img_right', [
        'label' => 'Image à droite',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => [],
        'wrapper' => [
            'width' => '',
            'class' => '',
            'id' => '',
        ],
        'message' => '',
        'default_value' => 0,
        'ui' => 0,
        'ui_on_text' => '',
        'ui_off_text' => '',
    ])
    ->addText('title')
    ->addText('content')
    ->addGroup('button')
        ->addText('label', ['wrapper' => $config->wrapper])
        ->addUrl('url', ['label' => 'URL', 'wrapper' => $config->wrapper])
    ->endGroup();


return $component;
