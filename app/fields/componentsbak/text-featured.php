<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object)[
    'ui' => 1,
    'wrapper' => ['width' => 30],
];

$component = new FieldsBuilder('text_featured');

$component
    ->addTrueFalse('hidden')
    ->addTab('content')
    ->addText('title')
    ->addText('subtitle')
    ->addTextarea('content')
    ->addTab('button')
    ->addGroup('button')
        ->addText('label', ['wrapper' => $config->wrapper])
        ->addUrl('url', ['label' => 'URL', 'wrapper' => $config->wrapper])
    ->endGroup();


return $component;
