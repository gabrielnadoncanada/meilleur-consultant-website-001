<?php
/**
 * Global Settings Page
 */

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}

$theme = new FieldsBuilder('Theme');

$theme
    ->setLocation('options_page', '==', 'theme-settings');

$theme
    ->addTab('header', [
        'placement' => 'left'
    ])
    ->addImage('logo')
    ->addTab('footer', [
        'placement' => 'left'
    ])
    ->addImage('footer_logo', [
        'label' => 'Logo'
    ])
    ->addWysiwyg('copyright')
    ->addTab('social', [
        'placement' => 'left'
    ])
    ->addUrl('facebook')
    ->addUrl('linkedin')
    ->addText('mail')
    ->addText('phone')
    ->addText('google_tag_manager');


if(get_modules()){
    $modules = get_modules();
    $theme->addTab('modules', [
        'placement' => 'left'
    ]);

    foreach ($modules as $key => $module){
        $theme->addTrueFalse($module['name'], ['ui' => 1]);
    }
}

return $theme;
